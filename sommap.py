from minisom import *
import numpy as np
import sys

def _normalize(x, bounds):
    dim = len(bounds)
    # x = np.atleast_2d(x).reshape(-1, dim)
    x = np.atleast_2d(x)
    b = np.atleast_2d(np.array(bounds)).T
    n = b[1] - b[0]
    ind = (n != 0)
    zind = (n == 0)
    r = np.empty([*x.shape])
    r[:,ind] = (x[:, ind] - b[0][None, ind]) / n[None, ind]
    r[:,zind] = 0.5
    return r
    
def _unnormalize(x, bounds):
    dim = len(bounds)
    # x = np.atleast_2d(x).reshape(-1, dim)
    x = np.atleast_2d(x)
    b = np.array(bounds).T
    return x * (b[1] - b[0]) + b[0]

def _build_iteration_indexes(data_len, num_iterations,
                             verbose=False, random_generator=None):
    """Returns an iterable with the indexes of the samples
    to pick at each iteration of the training.

    If random_generator is not None, it must be an instalce
    of numpy.random.RandomState and it will be used
    to randomize the order of the samples."""
    iterations = arange(num_iterations) % data_len
    if random_generator:
        random_generator.shuffle(iterations)
    if verbose:
        return _wrap_index__in_verbose(iterations)
    else:
        return iterations

class SomMap(MiniSom):

    # Initialization:   SomMap
    #     arg:  nsom:    (?)
    #           input_len:  (?)
    #           sigma:      (?)
    #           learning_rate:    learning_rate for SOM training
    #           decay_function:   decay function for decay function, refer to [minisom.py]
    #           neighborhood_function:   neighborhood relation, refer to [minisom.py]
    #           topology:         shape for SOM grid, refer to [minisom.py]
    #           activation_distance:       activation type [check for maximum relevant neuron], refer to [minisom.py]
    #           random seed
    #           bd:               bound (?)
    def __init__(self, nsom, input_len, sigma = 1.0, learning_rate = 0.5,
                                    decay_function = asymptotic_decay,
                                    neighborhood_function = 'gaussian', topology = 'rectangular',
                                    activation_distance = 'euclidean', random_seed = None, bd = None):
        super(SomMap, self).__init__(nsom[0], nsom[1], input_len = input_len,
                                    sigma = sigma,
                                    learning_rate = learning_rate,
                                    decay_function = decay_function,
                                    neighborhood_function = neighborhood_function,
                                    topology = topology,
                                    activation_distance = activation_distance,
                                    random_seed = random_seed)
        self._bd = bd
        self._debug = False
        self._folder = None 

    # Randomly initialize weights
    #   arg: data:  initialize data to weights
    def random_weights_init(self, data):
        """Initializes the weights of the SOM
        picking random samples from data."""
        self._check_input_len(data)
        it = nditer(self._activation_map, flags=['multi_index'])
        
        if self._bd is not None:
            data = _normalize(data, self._bd)     # normalize: x in R^d -> [-1, 1]^d
   
        self._data = data              
        while not it.finished:
            rand_i = self._random_generator.randint(len(data))
            self._weights[it.multi_index] = data[rand_i]
            it.iternext()
        self._init_weights = self._weights.copy()
        
    # Initialize weights by PCA
    def pca_weights_init(self, data):
        """Initializes the weights to span the first two principal components.

        This initialization doesn't depend on random processes and
        makes the training process converge faster.

        It is strongly reccomended to normalize the data before initializing
        the weights and use the same normalization for the training data.
        """
        if self._input_len == 1:
            msg = 'The data needs at least 2 features for pca initialization'
            raise ValueError(msg)
        
        if self._bd is not None:
            data = _normalize(data, self._bd)   # real domain [_bd] -> [0, 1] domain
        self._data = data

        self._check_input_len(data)
        if len(self._neigx) == 1 or len(self._neigy) == 1:
            msg = 'PCA initialization inappropriate:' + \
                  'One of the dimensions of the map is 1.'
            warn(msg)
        pc_length, pc = linalg.eig(cov(transpose(data)))
        pc_order = argsort(-pc_length)
        for i, c1 in enumerate(linspace(-0.5, 0.5, len(self._neigx))):
            for j, c2 in enumerate(linspace(-0.5, 0.5, len(self._neigy))):
                self._weights[i, j] = np.array([0.5, 0.5]) + c1*pc[pc_order[0]] + c2*pc[pc_order[1]]
        self._init_weights = self._weights.copy()
        
    # Get indices of inside layer nodes
    def _get_insider_idx(self):
        lx = len(self._neigx)
        ly = len(self._neigy)
        a = np.arange(lx)
        b = np.arange(ly)
        a, b = np.meshgrid(a, b)
        idx = np.hstack((a.reshape(lx * ly, 1), b.reshape(lx * ly, 1)))

        c1 = idx[:, 0] != 0
        c2 = idx[:, 0] != len(self._neigx) - 1
        c3 = idx[:, 1] != 0
        c4 = idx[:, 1] != len(self._neigy) - 1
        c = c1 * c2 * c3 * c4

        idx = idx[c]
        return idx
 
    # Get indices of outside layer nodes
    def _get_outsider_idx(self):
        lx = len(self._neigx)
        ly = len(self._neigy)
        a = np.arange(lx)
        b = np.arange(ly)
        a, b = np.meshgrid(a, b)
        idx = np.hstack((a.reshape(lx * ly, 1), b.reshape(lx * ly, 1)))

        c1 = idx[:, 0] == 0
        c2 = idx[:, 0] == len(self._neigx) - 1
        c3 = idx[:, 1] == 0
        c4 = idx[:, 1] == len(self._neigy) - 1
        c = c1 + c2 + c3 + c4

        idx = idx[c]
        return idx

    # Get indices of 2nd-stage outside layer nodes
    def _get_outsider_2nd_idx(self):
        lx = len(self._neigx)
        ly = len(self._neigy)
        a = np.arange(lx)
        b = np.arange(ly)
        a, b = np.meshgrid(a, b)
        idx = np.hstack((a.reshape(lx * ly, 1), b.reshape(lx * ly, 1)))

        c1 = idx[:, 0] == 1
        c2 = idx[:, 0] == len(self._neigx) - 2
        c3 = idx[:, 1] == 1
        c4 = idx[:, 1] == len(self._neigy) - 2
        c = c1 + c2 + c3 + c4

        idx = idx[c]
        return idx

    # Extend outsider from 2nd-stage outsider to boundary
    def _extend_outsider(self):
      
        lx = len(self._neigx) - 1
        ly = len(self._neigy) - 1

        idx = self._get_outsider_2nd_idx()
        idx_in = self._get_insider_idx()
            # print('idx', idx)

        center = self._weights.reshape(len(self._neigx) * len(self._neigy), -1)
        center = np.mean(center, axis = 0)
        w = self._weights[idx.T[0], idx.T[1]]
        v = w - center
        lambda_1 = -w/v
        lambda_2 = (1-w)/v
        lambda_0 = np.hstack((lambda_1, lambda_2))
        lambda_0[np.where(lambda_0 < 0)] = np.inf
        lambda_3 = np.min(lambda_0, axis = 1)
        edgepoint = np.einsum('i,ij->ij', lambda_3, v) + w
        edgepoint[edgepoint < 0.0] = 0.0
        edgepoint[edgepoint > 1.0] = 1.0

        # compute immediate outsider
        idx_out = idx
        idx_out[:, 0][idx_out[:,0] == 1] = 0
        idx_out[:, 0][idx_out[:,0] == (lx - 1)] = lx
        idx_out[:, 1][idx_out[:,1] == 1] = 0
        idx_out[:, 1][idx_out[:,1] == (ly - 1)] = ly
        self._weights[idx_out.T[0], idx_out.T[1]] = edgepoint
        
        # minor adjust
        lx = len(self._neigx) - 1
        ly = len(self._neigy) - 1
        self._weights[1, 0]         = self._weights[0, 0]
        self._weights[0, 1]         = self._weights[0, 0]
        self._weights[lx - 1, 0]    = self._weights[lx, 0]
        self._weights[lx, 1]        = self._weights[lx, 0]
        self._weights[lx - 1, ly]   = self._weights[lx, ly] 
        self._weights[lx, ly - 1]   = self._weights[lx, ly]
        self._weights[1, ly]        = self._weights[0, ly]
        self._weights[0, ly - 1]    = self._weights[0, ly]

        # print('weight', self._weights[idx.T[0], idx.T[1]])

    # Activation function
    def _activate(self, x):
        self._activation_map = np.zeros((len(self._neigx), len(self._neigy)))
        self._activation_map[1:len(self._neigx)-1, 1:len(self._neigy)-1] =      self._activation_distance(x, self._weights[1:len(self._neigx) - 1, 1:len(self._neigy) - 1])

    # Find winner upon outputs from activation function
    def winner(self, x):
        self._activate(x)
        insider = self._activation_map[1:len(self._neigx)-1, 1:len(self._neigy)-1]
        idx = unravel_index(insider.argmin(), insider.shape)
        idx = tuple(np.add(idx, 1))
        return idx

    def train(self, data, pivot, num_iteration, random_order=False, verbose=False, epsilon = 1e-2):
    
        """Trains the SOM.

        Parameters
        ----------
        data : np.array or list
            Data matrix.

        num_iteration : int
            Maximum number of iterations (one iteration per sample).
        random_order : bool (default=False)
            If True, samples are picked in random order.
            Otherwise the samples are picked sequentially.

        verbose : bool (default=False)
            If True the status of the training
            will be printed at each iteration.
        """
  
        if self._bd is not None: 
            self.data = _normalize(data, self._bd)
            self.pivot = _normalize(pivot, self._bd)[0]

        self._check_iteration_number(num_iteration)
        self._check_input_len(self.data)
        random_generator = None
        if random_order:
            random_generator = self._random_generator
        iterations = _build_iteration_indexes(len(self.data), num_iteration,
                                              verbose, random_generator)

        for t, iteration in enumerate(iterations):
        
            self.update(self.data[iteration], self.winner(self.data[iteration]),
                        t, num_iteration)
        
            if (t % 20 == 0) and (self._debug):
                self.plot_grid3(self._weights, t)

        # t = 0
        # while (np.sum(self.mapx_mask(self.data)) < (0.5 * len(self.data))) and (t < num_iteration):
        #     iteration = iterations[t]
        #     self.update(self.data[iteration], self.winner(self.data[iteration]),
        #                 t, num_iteration)
        #     if (t % 20 == 0) and (self._debug):
        #         self.plot_grid3(self._weights, t)
        #     t = t + 1
        #     print('')
        #     print('t', t, ' - map', np.sum(self.mapx_mask(self.data)))
        self._extend_outsider()
 
        if verbose:
            print('\n quantization error:', self.quantization_error(self.data))
         
   
    def find_mapx(self, x, epsilon = 1e-2):
        """Check if mapped points fall on map

        Parameters
        ----------
        x : point on real space

        epsilon : maximum error allowed to judge points on map
        
        """
        if self._bd is not None:
            x = _normalize(x, self._bd)

        d = npmin(sqrt(npsum((self._weights[:,:,None,:] - x)**2, axis = -1)), axis = -1)
        return d < epsilon

    def get_delta(self, x):
        return sqrt(npsum((self._weights[:,:,None,:] - x)**2, axis = -1))

    def remove_edge_data(self, mx, my):
        
        mx_n = np.empty([0, 2])
        my_n = np.empty([0, 1])
        for i in np.arange(len(mx)):
            if mx[i, 0] != 0 and mx[i, 0] != 1 and mx[i, 1] != 0 and mx[i, 1] != 1:
                mx_n = np.vstack((mx_n, mx[i]))
                my_n = np.vstack((my_n, my[i]))
        
        return mx_n, my_n

    def mapx(self, x, f, epsilon = 1e-2):
        
        if self._bd is not None:
            x = _normalize(x, self._bd)
        delta = self.get_delta(x)
        xi = npargmin(delta, axis = -1)
        mask = (npmin(delta, axis = -1) < epsilon)
        mx = array(npwhere(mask)).T/array([len(self._neigx) - 1, len(self._neigy) - 1])
        mf = f[xi][mask]
        return mx, mf
    
    def mapx_only(self, x, epsilon = 1e-2):
        
        if self._bd is not None:
            x = _normalize(x, self._bd)

        delta = self.get_delta(x)
        xi = npargmin(delta, axis = -1)
        mask = (npmin(delta, axis = -1) < epsilon)
        mx = array(npwhere(mask)).T/array([len(self._neigx) - 1, len(self._neigy) - 1])
        return mx

    def mapx_mask(self, x, epsilon = 1e-2):

        if self._bd is not None:
            x = _normalize(x, self._bd)        

        delta = self.get_delta(x)
        mask = (npmin(delta, axis = -1) < epsilon)
        return mask

    def get_id_ongrid(self, epsilon = 1e-2):
        mask = self.mapx_mask(self._data, epsilon)
        delta = self.get_delta(self._data)
        xi = npargmin(delta, axis = -1)
        return unique(xi[mask])
    
    def unmapx(self, x):
        x = atleast_2d(x)

        # map from normalized space to grid space 
        # [0, 1] & [0, 1] -> [0, x] & [0, y]
        nx = x * array([len(self._neigx) - 1, len(self._neigy) - 1])

        x0 = floor(nx).astype(int)
        k = nx - x0

        h0 = x0[:, 0]
        v0 = x0[:, 1]
        h1 = x0[:, 0] + 1
        v1 = x0[:, 1] + 1
        h1[h1 > len(self._neigx) - 1] = len(self._neigx) - 1
        v1[v1 > len(self._neigy) - 1] = len(self._neigy) - 1
        x1 = self._weights[h0, v0, :]
        x2 = self._weights[h1, v0, :]
        x3 = self._weights[h1, v1, :]
        x4 = self._weights[h0, v1, :]
        xh1 = x1 + k[:, 0][:, None] * (x2 - x1)
        xh2 = x4 + k[:, 0][:, None] * (x3 - x4)
        xr = xh1 + k[:, 1][:, None] * (xh2 - xh1)

        if self._bd is not None:
            xr = _unnormalize(xr, self._bd)
        return xr
    
    def plot_grid3(self, data, idx):

        from mpl_toolkits.mplot3d import axes3d
        import matplotlib.pyplot as plt

        # cx & cy & cz meshgrid
        m = len(self._neigx)

        # if self._bd is not None:
        #     data = _normalize(data, self._bd)

        if(len(data.T) < 3):
            data_n = zeros([data.shape[0], data.shape[1], 3])
            data_n[:, :, :2] = data
        else:
            data_n = data

        # debug
        self.data_n = data_n

        cx = data_n.T[0].reshape(m, -1)
        cy = data_n.T[1].reshape(m, -1)
        cz = data_n.T[2].reshape(m, -1)
        
        # draw figure
        fig = plt.figure(figsize = (10, 10))
        ax = fig.add_subplot(111, projection = '3d')
        ax.plot_wireframe(cx, cy, cz, zorder = 1)

        # scatter data points
        scatter_n = zeros([self._data.shape[0], 3])
        if(self._data.shape[1] < 3):
            scatter_n[:, :self._data.shape[1]] = self._data
        else:
            scatter_n[:, :] = self._data[:, :2]

        # find on-grid and off-grid scatter points
        xi_on = self.get_id_ongrid()
        xi_off = [element for i, element in enumerate(arange(self._data.shape[0])) if i not in xi_on]

        ax.scatter(*(scatter_n[xi_off].T), color = 'white', marker = '^', edgecolor = 'k', zorder = 2, s = 50)
        # ax.scatter(*(self._data.T), color = 'white', marker = '^', edgecolor = 'black', zorder = 2)
        figname = 'fig' + str(idx) + '.png'

        # highlight scatters on grid
        ax.scatter(*(scatter_n[xi_on].T), color = 'black', marker = '^', edgecolor = 'black', zorder = 3, s = 50)
        
        import os.path
        
        # create a subset
        if idx == 0:
            folder_id = 0
            folder = 'set' + str(folder_id)
            folder = os.path.join(self.debug_folder, folder)
            while os.path.isdir(folder):
                folder_id += 1
                folder = 'set' + str(folder_id)
                folder = os.path.join(dirs, folder)
            os.makedirs(folder)
            self._folder = folder

        fpath = os.path.join(self._folder, figname)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        title_name = "SOM training-iteration " + str(idx)
        ax.set_title(title_name)
        plt.locator_params(nbins = 4)
        ax.view_init(elev=70, azim = -90)
        plt.savefig(fpath)
        plt.close()
        
    def set_debug(self, debug = False, debug_folder = None):
        self._debug = debug
        if not debug_folder:
            if not os.path.isdir('exp'):    
                os.makedirs('exp')
            self.debug_folder = get_active_folder('exp', 'set')
            self.debug_folder = os.path.join(self.debug_folder, 'fitting')
            os.makedirs(self.debug_folder)
        else:
            self.debug_folder = os.path.join(debug_folder, 'fitting')
            if not os.path.isdir(self.debug_folder):
                os.makedirs(self.debug_folder)
            
        self._debug_folder = debug_folder


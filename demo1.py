from gpsomopt import *
import GPyOpt
import os
import shutil

parent_dir = 'gpsomopt_raw'
if os.path.isdir(parent_dir):
    shutil.rmtree(parent_dir)
os.mkdir(parent_dir)

f_class = GPyOpt.objective_examples.experiments2d.branin
f_true = f_class()
bounds = [{'name':'var_1', 'type': 'continuous', 'domain': f_true.bounds[0]}
        , {'name':'var_2', 'type': 'continuous', 'domain': f_true.bounds[1]}]
myopt = GPSomOpt(f_true.f, bounds, subset_numdata = 20)

# figname = 'fig' + str(0) + '.png'
# fullname = os.path.join(parent_dir, figname)
# myopt.draw_fig(fullname)

myopt.run(debug = True)
# myopt.myopt.plot_convergence(filename = 'convergence.png')

import os

def get_active_folder(dirs, folder_name):
    fid = 0
    dir_name = os.path.join(dirs, folder_name + str(fid))
    while os.path.isdir(dir_name):
        fid += 1
        dir_name = os.path.join(dirs, folder_name + str(fid))
    os.makedirs(dir_name)

    return dir_name

def get_active_filename(dirs, file_name):
    fid = 0
    full_file = os.path.join(dirs, file_name + str(fid))
    while os.path.isfile(full_file):
        fid += 1
        full_file = os.path.join(dirs, file_name + str(fid))
    return full_file

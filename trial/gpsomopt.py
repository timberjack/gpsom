import numpy as np
import logging
import os
import matplotlib.pyplot as plt
import matern_som
from GPyOpt.core.task.space import Design_space
from GPyOpt.experiment_design import initial_design
from pylab import savefig
from shepard import *
from sommap import *
from somoptpatch import SomOptPatch
from util import *
import time

class GPSomOpt:

    def __init__(self, f, domain, constraints = None, x = None, y = None, gxmin = None, gymin = np.inf, maximize = False, initial_design_numdata = 20, model_update_interval = 1, subset_numdata = 20, gp_numdata = 5, ntrial_som_fit = 1000):
        self.f = f
        self.domain = domain
        self.constraints = constraints
        self.space = Design_space(self.domain, self.constraints)
        self.bd = np.asarray(self.space.get_bounds())
        self.dim = len(self.bd)
        if not x or not y:
            self.x = np.empty(shape = [0, self.dim])
            self.y = np.empty(shape = [0, 1])
        else:
            self.x = x
            self.y = y
        self.gxmin = gxmin
        self.gymin = gymin
        self.maximize = maximize
        self.initial_design_numdata = initial_design_numdata
        if self.initial_design_numdata < 5:
            self.initial_design_numdata = 5
        self.model_update_inteval = model_update_interval
        self.subset_numdata = subset_numdata
        self.gp_numdata = gp_numdata
        self.ntrial_som_fit = ntrial_som_fit        
        self.myopt = None
        self.px = None
        self.py = None  
        self.logger = logging.getLogger('gpsom')
        if len(self.x) < self.initial_design_numdata:
            num = self.initial_design_numdata - len(self.x)
            x_extra = initial_design('random', self.space, num)
            y_extra = self.f(x_extra)
            self.x = np.vstack([self.x, x_extra])
            self.y = np.vstack([self.y, y_extra])

    # plot real function in SOM grid view
    def plot_map(self, sx):

        if self.dim != 2:
            return

        # mapped plot
        R1 = np.linspace(0, 1, 200)
        R2 = np.linspace(0, 1, 200)
        r1, r2 = np.meshgrid(R1, R2)
        r = np.hstack((r1.reshape(200 * 200, 1), r2.reshape(200 * 200, 1)))
        x = self.som.unmapx(r)
        y = self.f(x)
        Y = y.reshape(200, 200)
        plt.contourf(R1, R2, Y, 100)

        # neurons
        w = self._unnormalize(self.som._weights, self.bd)
        a = w.shape[0]
        b = w.shape[1]
        mask = self.som.mapx_mask(sx)

        for i in np.arange(a):
            for j in np.arange(b):
                if i+1 < a:
                    p = [i/(a - 1), j/(b - 1)]
                    q = [(i + 1)/(a - 1), j/(b - 1)]
                    plt.plot([p[0], q[0]], [p[1], q[1]], 'w')
                if j+1 < b:
                    p = [i/(a - 1), j/(b - 1)]
                    q = [i/(a - 1), (j + 1)/(b - 1)]
                    plt.plot([p[0], q[0]], [p[1], q[1]], 'w')

        for i in np.arange(a):
            for j in np.arange(b):
                if mask[i][j]:
                    p = [i/(a - 1), j/(b - 1)]
                    plt.plot(p[0], p[1], 'r.', markersize = 10)

    # plot 2d SOM grid
    def plot_2d_grid(self, w):
    
        if self.dim != 2:
            return

        w = self._unnormalize(w, self.bd)
        a = w.shape[0]
        b = w.shape[1]
        for i in np.arange(a):
            for j in np.arange(b):
                if i+1 < a:
                    p = w[i][j]
                    q = w[i+1][j]
                    plt.plot([p[0], q[0]], [p[1], q[1]], 'w')
                if j+1 < b:
                    p = w[i][j]
                    q = w[i][j+1]
                    plt.plot([p[0], q[0]], [p[1], q[1]], 'w')

    # check if kernel works correctly
    def check1(self):
        input_dim = len(self.bd)
        if input_dim == 2:
            X1 = np.linspace(0, 1, 200)
            X2 = np.linspace(0, 1, 200)
            x1, x2 = np.meshgrid(X1, X2)
            X = np.hstack((x1.reshape(200 * 200, 1), x2.reshape(200 * 200, 1)))
            acqu = self.myopt.acquisition.acquisition_function(X)
            Xmap = self.som.unmapx(X)
            real = self.f(Xmap)

            # compute percentage
            percent = np.sum(acqu < real)/len(X)
            print('percent', percent)

    def plot_real_vs_som(self, filename = None, label_x = None, label_y = None):

        input_dim = len(self.bd)
        if input_dim == 2:
            if not label_x:
                label_x = 'X1'
            if not label_y:
                label_y = 'X2'

        # compute real values
        X1 = np.linspace(self.bd[0][0], self.bd[0][1], 200)
        X2 = np.linspace(self.bd[1][0], self.bd[1][1], 200)
        x1, x2 = np.meshgrid(X1, X2)
        x = np.hstack((x1.reshape(200 * 200, 1), x2.reshape(200 * 200, 1)))
        y = self.f(x)
        Y = y.reshape(200, 200)
        w = self.som._weights
        w_init = self.som._init_weights

        # plot while figure
        plt.figure(figsize = (15, 5))

        # plot 1st figure
        plt.subplot(1,3,1)
        plt.contourf(X1, X2, Y, 100)
        plt.plot(self.sx[:, 0], self.sx[:, 1], 'r.', markersize = 10, label = u'Observations')
        self.plot_2d_grid(w_init)
        plt.title('SOM grid without fitting')
        plt.xlabel(label_x)
        plt.ylabel(label_y)
        plt.colorbar()

        # plot 1st figure
        plt.subplot(1,3,2)
        plt.contourf(X1, X2, Y, 100)
        plt.plot(self.sx[:, 0], self.sx[:, 1], 'r.', markersize = 10, label = u'Observations')
        self.plot_2d_grid(w)

        plt.title("Function value on real domain")
        plt.xlabel(label_x)
        plt.ylabel(label_y)
        plt.colorbar()

        # plot 2nd figure
        plt.subplot(1,3,3)
        R1 = np.linspace(0, 1, 200)
        R2 = np.linspace(0, 1, 200)
        r1, r2 = np.meshgrid(R1, R2)
        r = np.hstack((r1.reshape(200 * 200, 1), r2.reshape(200 * 200, 1)))
        x = self.som.unmapx(r)
        y = self.f(x)
        Y = y.reshape(200, 200)
        plt.contourf(R1, R2, Y, 100)
        self.plot_map(self.sx)
        plt.title("Function value mapped on SOM")
        plt.xlabel(label_x)
        plt.ylabel(label_y)
        plt.colorbar()

        # if filename provided, save it; otherwise, show it
        if filename != None:
            savefig(filename)
        else:
            plt.show()

    def select_subset(self, px, f_select = get_fitness5, eps = 1e-3, galpha = 1.0):
        
        print('select from whole set', len(self.x))
        self.sx, self.sy = select_subset_debug(self.x, self.y, self.px, f_select, bd = self.bd, N_min = 10, N_max = 20, g_alpha = galpha)
        print('select ', len(self.sx), ' samples as subset')
        return self.sx, self.sy

    def draw_a_point(self):
        self.px = initial_design('random', self.space, 1)[0]
        print('draw a point')
        return self.px

    def draw_subset(self):
       
        if not os.path.isdir('subset'):
            os.makedirs('subset')
        folder_name = get_active_folder('subset', 'set')
        self.subset_folder = folder_name
        file_name = os.path.join(self.subset_folder, 'subset')

        plt.figure(figsize = (10, 5))
        plt.subplot(1,2,1)
        bd = self.space.get_bounds()
        X1 = np.linspace(bd[0][0], bd[0][1], 200)
        X2 = np.linspace(bd[1][0], bd[1][1], 200)
        x1, x2 = np.meshgrid(X1, X2)
        X = np.hstack((x1.reshape(200 * 200, 1), x2.reshape(200 * 200, 1)))
        Y = self.f(X)
        cs1 = plt.contour(x1, x2, Y.reshape(200,  200), 10, alpha = 0.3)
        plt.clabel(cs1, inline = 1)

        # draw subset
        plt.plot(self.x[:, 0], self.x[:, 1], 'r.', markersize = 10)
        plt.plot(self.px[0], self.px[1], 'r.', markersize = 10, zorder = 1)
        plt.plot(self.px[0], self.px[1], 'k+', markersize = 20, zorder = 2)
        plt.xlabel('x1')
        plt.ylabel('x2')
        plt.title('Start point and existing samples')

        plt.subplot(1,2,2)
        # draw real plot
        bd = np.asarray(self.space.get_bounds())
        X1 = np.linspace(bd[0][0], bd[0][1], 200)
        X2 = np.linspace(bd[1][0], bd[1][1], 200)
        x1, x2 = np.meshgrid(X1, X2)
        X = np.hstack((x1.reshape(200 * 200, 1), x2.reshape(200 * 200, 1)))
        Y = self.f(X)
        cs2 = plt.contour(x1, x2, Y.reshape(200,  200), 10, alpha = 0.3)
        plt.clabel(cs2, inline = 1)
        # plt.colorbar()

        # draw subset
        # sx, sf = self.select_subset(self.px, f_select = get_fitness2)
        plt.plot(self.sx[:, 0], self.sx[:, 1], 'r.', markersize = 15)
        gx = gradient_shepard_v3(self.x, self.y, self.px, bd = bd, N_min = 5, N_max = 10)
        plt.quiver(self.px[0], self.px[1], gx[0], gx[1], zorder = 3)
        plt.plot(self.px[0], self.px[1], 'r.', markersize = 15, zorder = 1)
        plt.plot(self.px[0], self.px[1], 'k+', markersize = 20, zorder = 2)

        plt.xlabel('x1')
        plt.ylabel('x2')
        plt.title('Start point and selected sample subsets')
        savefig(file_name)

    def draw_real_to_som(self):

        if not os.path.isdir('r2s'):
            os.makedirs('r2s')
        folder_name = get_active_folder('r2s', 'set')
        self.r2s_folder = folder_name

        file_name = os.path.join(self.r2s_folder, 'r2s')
        self.plot_real_vs_som(file_name)

    def do_wrapf(self):
        
        # wrap true function
        from functools import wraps
        def som_map(func):
            @wraps(func)
            def wrapper(x):
                x = self.som.unmapx(x)
                return func(x)
            return wrapper
        self.f_wrap = som_map(self.f)
        return self.f_wrap

    def get_gradient(self):
        return gradient_shepard_v3(self.x, self.y, self.px, self.bd, N_min = 5, N_max = 10, epsilon = 1e-10)

    def fit_som(self, nsom, random_seed = None, verbose = False, debug = False):

        print('fit som')
        self.som = SomMap(nsom, self.dim, sigma = 1.5, learning_rate = .5,
                    neighborhood_function = 'gaussian', random_seed = random_seed, bd = self.bd)
        sx_n = np.vstack([self.sx, self.px])
        self.som.random_weights_init(sx_n)
        self.som.set_debug(True)
        self.som.train(sx_n, self.px, self.ntrial_som_fit, verbose)
        self.mx, self.my = self.som.mapx(self.sx, self.sy, 1e-2)
        self.do_wrapf()

    @staticmethod
    def _normalize(x, bounds):
        dim = len(bounds)
        x = np.atleast_2d(x)
        b = np.atleast_2d(np.array(bounds)).T
        n = b[1] - b[0]
        ind = (n != 0)
        zind = (n == 0)
        r = np.empty([*x.shape])
        r[:,ind] = (x[:, ind] - b[0][None, ind]) / n[None, ind]
        r[:,zind] = 0.5
        return r

    @staticmethod
    def _unnormalize(x, bounds):
        dim = len(bounds)
        x = np.atleast_2d(x)
        b = np.array(bounds).T
        return x * (b[1] - b[0]) + b[0]

    def init_optimize(self, debug = False):
        
        unit_bound = (0, 1)
        mbd = [{'name': 'var', 'type':'continuous', 'domain':unit_bound, 'dimensionality': 2}]
        mspace = Design_space(mbd, None)
        if len(self.mx) < self.gp_numdata:
            num_extra = self.gp_numdata - len(self.mx)
            x_extra = initial_design('random', mspace, num_extra)
            x_extra_unmap = self.som.unmapx(x_extra)
            y_extra = self.f(x_extra_unmap)
            self.x = np.vstack((self.x, x_extra_unmap))
            self.y = np.vstack((self.y, y_extra))
            self.mx = np.vstack((self.mx, x_extra))
            self.my = np.vstack((self.my, y_extra))
        self.myopt = SomOptPatch(dim = self.dim,
                                     som = self.som,
                                     f = self.f_wrap,
                                     domain = mbd,
                                     acquisition_type = 'LCB',
                                     exact_feval = True,
                                     X = self.mx,
                                     Y = self.my,
                                     gxmin = None,
                                     gymin = np.inf,
                                     model_update_interval = 10,
                                     bd = self.bd,
                                     acquisition_weight = 3,
                                     ARD = True)

        # plots folder
        if self.debug:    
            if not os.path.isdir('exp'):
                os.makedirs('exp')
            folder_name = get_active_folder('exp', 'set')
            self.subset_folder = folder_name

    def step_optimize(self, ntrial = 10, max_time = 60, eps = 1e-5, minstep = 1e-2, debug = False):
       
        self.gxmin, self.gymin = self.myopt.run_optimization(max_iter = ntrial, max_time = max_time, gxmin = self.gxmin, gymin = self.gymin, eps = eps, debug = debug)

    def compute_result(self):
        
        self.x_opt = self.x[np.argmin(self.y)]
        self.fx_opt = np.min(self.y)
        if self.gymin > self.fx_opt:
            self.gxmin = self.x_opt
            self.gfmin = self.fx_opt
        self.logger.info('Minimum x' + str(np.round((self.x_opt), 3)))
        self.logger.info('Minimum y' + str(np.round((self.fx_opt), 3)))

    def conclude_optimize(self):
        
        print('optimization concludes')
        self.newx = self.som.unmapx(self.myopt.xnew)
        self.newy = self.myopt.ynew
        self.x = np.vstack((self.x, self.newx))
        self.y = np.vstack((self.y, self.newy))
        newi = np.argmin(self.newy)
        if self.pf > self.newy[newi]:
            self.px = self.newx[newi]
            self.pf = self.newy[newi]        

    def run_optimize(self, ntrial = 10, eps = 1e-2, debug = False):

        self.init_optimize()
        self.step_optimize(ntrial = ntrial, eps = eps, max_time = np.inf, debug = debug)
        self.conclude_optimize()

    def run(self, sp_num = 1, ntrial = 10, eps = 1e-2, debug = False):

        if self.dim != 2 and debug == True:
            debug = False
    
        for i in np.arange(sp_num):

            # initially randomly draw a start point px
            self.px = initial_design('random', self.space, 1)[0]
            self.pf = np.inf

            # compute gradient of start point px
            g = np.linalg.norm(self.get_gradient())
            self.logger.info('Get gradient of px ' + str(round(g, 3)))

            count = 0

            while g > 0:

                self.select_subset(self.px, get_fitness5)
                self.logger.info('Find a subset of samples.')
                if debug:
                    self.draw_subset()

                # fit som
                self.logger.info('Fit SOM...')
                nsom = np.array([10, 10])
                ntrial = 100
                self.fit_som(nsom, verbose = True, debug = True)
                if debug:
                    self.draw_real_to_som()                

                # optimize
                self.logger.info('Optimize by GP-LCB...')
                self.run_optimize(ntrial = ntrial, eps = eps, debug  = True)
            
                # show result
                self.compute_result()

                # re-compute gradient of start point px
                g = np.linalg.norm(self.get_gradient())
                self.logger.info('Get gradient of px ' + str(round(g, 3)))

                print('g', g)
                print('---------------------------------------------------------')

            # gradient is small enough, exit loop
            self.logger.info('Terminate search due to small gradient')


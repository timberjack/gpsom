import GPy
from GPy.kern.src.stationary import Matern52
import numpy as np
from sommap import SomMap

class MaternSOM52(Matern52):

    def __init__(self, input_dim, som: SomMap):
        super(MaternSOM52, self).__init__(input_dim = input_dim, name = 'MatSOM52')
        self.som = som
    
    def K(self, X, X2 = None):
        X = self.som.unmapx(X)
        if X2 is not None:
            X2 = self.som.unmapx(X2)
        r = self._scaled_dist(X, X2)
        return self.K_of_r(r)

    def to_dict(self):
        input_dict = super(MaternSOM52, self).save_to_input_dict()
        input_dict["class"] = "GPy.kern.MaternSOM52"
        return input_dict


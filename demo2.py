from gpsomopt import *
import GPyOpt
import os
import shutil
from GPyOpt.core.task.space import *
parent_dir = 'gpsomopt_raw'
if os.path.isdir(parent_dir):
    shutil.rmtree(parent_dir)
os.mkdir(parent_dir)

f_class = GPyOpt.objective_examples.experiments2d.branin
f_true = f_class()
bounds = bounds_to_space(f_true.bounds)
myopt = GPSomOpt(f_true.f, bounds, subset_numdata = 20, ntrial_som_fit = 1000)
# figname = 'fig' + str(0) + '.png'
# fullname = os.path.join(parent_dir, figname)
# myopt.draw_fig(fullname)
myopt.run(ntrial = 20, debug = True)
# myopt.myopt.plot_convergence(filename = 'convergence.png')

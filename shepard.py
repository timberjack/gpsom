import numpy as np
import sys
from sys import exit

def _unnormalize(x, bounds):
    dim = bounds.shape[0]
    x = np.atleast_2d(x).reshape(-1, dim)
    b = np.array(bounds).T
    m = 0.5 * (b[1] + b[0])
    n = 0.5 * (b[1] - b[0])
    return x * n + m
    # return (x * (b[1] - b[0]) + b[0])

def _normalize(x, bounds):
    dim = bounds.shape[0]
    x = np.atleast_2d(x).reshape(-1, dim)
    b = np.atleast_2d(np.array(bounds)).T
    m = 0.5 * (b[1] + b[0])
    n = 0.5 * (b[1] - b[0])
    ind = (n != 0)
    zind = (n == 0)
    r = np.empty([*x.shape])
    r[:,ind] = (x[:,ind] - m[None, ind])/n[None, ind]
    r[:,zind] = 0
    return r

# distance from p to all points in d using Euclidean distance
def dist_2_all(d, p):
    return np.sqrt(np.sum((p - d)**2, axis = 1))

def dist(d, p):
    return np.sqrt(np.sum(p - d)**2)

# shepard surface original version (IDW)
def weight_idw(d, u = 1, epsilon = 0.001):
    
    if u < 1:
        u = 1

    if(d < epsilon):
        w = 1/epsilon
    else:
        w = 1/d
    return np.power(w, u)
    
def weight_idw_all(sx, sf, px, u = 1, epsilon = 0.001):
    
    # compute weights
    d = dist_2_all(sx, px)
    w = np.zeros(len(d))
    for i in np.arange(len(d)):
        w[i] = weight_idw(d[i], u = u, epsilon = epsilon)
    return w, sx, sf

def shepard_idw(sx, sf, px, bd = None, u = 1, epsilon = 0.001):
    
    # if points close to px exist, compute pf from sf
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]
  
    d = dist_2_all(sx, px)
    di = np.where(d < epsilon)[0]
    if(len(di) > 0):
        minidx = di[np.argmin(d[di])]
        return sf[minidx]
    
    # of no close points, compute in idw
    w, sx, sf = weight_idw_all(sx, sf, px, u = u, epsilon = epsilon)
    pf = np.sum(w * sf.T)/np.sum(w)
    return pf

def gradient_shepard_idw_1(sx, sf, px, bd = None, u = 1, epsilon = 1e-3):
    
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    if u < 1:
        print('power u should be higher than 1')
        u = 1

    # init gradient
    dim = len(sx[0])
    g = np.zeros(dim)

    d = dist_2_all(sx, px)
    di = np.where(d >= epsilon)[0]
    if len(di) == 0:
        return g
    sx = sx[di]
    sf = sf[di]
    d = d[di]

    # compute gradients
    N = len(sx)
    
    denormi = np.sum(np.power(d, -u))**2

    for di in np.arange(dim):
        for i in np.arange(N):
            for j in np.arange(N):
                if i == j:
                    continue
                g[di] += np.power(d[i], -u-2) * np.power(d[j], -u) * (px[di] - sx[i, di]) * sf[i] * (sf[i] - sf[j])
        g[di] = g[di]/denormi
    
    return g
                
# gradient
def gradient_shepard_idw(sx, sf, px, bd = None, u = 1, epsilon = 0.001):
    
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    if u < 1:
        print('power u should be higher than 1')
        u = 1

    # init gradient
    dim = len(sx[0])
    g = np.zeros(dim)

    # compute pf
    pf = shepard_idw(sx, sf, px, u = 1, epsilon = epsilon)

    # get rid of near-by scatters
    d = dist_2_all(sx, px)
    di = np.where(d >= epsilon)[0]
    if len(di) == 0:
        return g
    sx = sx[di]
    sf = sf[di]
    d = d[di]
 
    # compute weights
    w, sx, sf = weight_idw_all(sx, sf, px, u = u, epsilon = epsilon)
    # compute gradeitns
    for di in np.arange(dim):
        normi = 0
        denormi = np.sum(w)
        for i in np.arange(len(sx)):
            normi += w[i] * (sf[i] - pf) * (sx[i, di] - px[di])/(d[i]**2)
        g[di] = normi/denormi
    return g

from shepard import *
import numpy as np

def compute_r(dim, N, N_near):
    normi = 1
    a = dim / 2
    while(a > 0):
        normi *= a
        a = a - 1
    denormi = np.power(np.pi, np.floor_divide(dim, 2))
    return np.power(np.power(2, dim) * N_near * normi/(denormi * N), 1/dim)

def compute_rn(n, d):
    d.sort()
    n = min(len(d) - 1, n)
    n = int(max(0, n))
    return d[n]

def weight_v1(d, r):
    
    if d < 0:
        sys.exit('Error: distance needed to be larger than 0')

    if(d > 0 and d <= r/3):
        return 1/d
    elif(d > r/3 and d <= r):
        return (27/(4 * r)) * (d/r - 1)**2
    else:
        return 0
    
def weight_v1_all(sx, sf, px, N_min, N_max):
    
    # select scatter subset
    sx, sf, rn = select_scatter_v1(sx, sf, px, N_min, N_max)

    # compute weights
    d = dist_2_all(sx, px)
    w = np.zeros(len(d))
    for i in np.arange(len(d)):
        w[i] = weight_v1(d[i], rn)

    return w, sx, sf

def select_scatter_v1(sx, sf, px, N_min, N_max):

    # recheck N_min, N_max, compute N_near
    if N_min is None:
        N_min = len(sx)
    
    if N_max is None:
        N_max = len(sx)    

    if N_min > N_max:
        N_min, N_max = N_max, N_min

    N_min = max(2, N_min)
    N_max = min(len(sx), N_max)
    N_near = int(0.5 * (N_min + N_max))

    # compute distance from px to all sx(s)
    d = dist_2_all(sx, px) 
    
    # compute radius (use in shepard version 1)
    dim = len(sx[0])
    r = compute_r(dim, len(sx), N_near)
 
    # decide number of scatter involved within r
    n = np.sum(d < r)
    if(n < N_min):
        n = N_min
    elif(n > N_max):
        n = N_max
    n = int(min(n, len(d)))
    idx = d.argsort()
    sx = sx[idx[:n]]
    sf = sf[idx[:n]]

    # decide r_prime
    rn = compute_rn(n, d)

    return sx, sf, rn

def shepard_v1(sx, sf, px, bd = None, N_min = None, N_max = None, epsilon = 1e-3):
    
    # if points close to px exist, compute pf from sf
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    # otherwise continue, use shepard version 1
    w, sx, sf = weight_v1_all(sx, sf, px, N_min = N_min, N_max = N_max)    
    d = dist_2_all(sx, px)   

    # if some sx(s) are very close to px (d < epsilon), use its value (sf)
    di = np.where(d < epsilon)[0]
    if(len(di) > 0):
        minidx = di[np.argmin(d[di])]
        return sf[minidx]
 
    # if sum of w is zero, compute pf from average of sf
    denormi = np.sum(w**2)
    if(denormi == 0.0):
        pf = np.average(sf)
    else:
        pf = np.sum((w**2) * sf.T)/denormi
    return pf

# gradient
def gradient_shepard_v1(sx, sf, px, bd = None, N_min = None, N_max = None, epsilon = 1e-3):
 
    # _normalize sx if bound is provided
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]
    elif px.ndim > 1:
        px = px[0]
        

    # init gradients
    dim = len(sx[0])
    g = np.zeros(dim)

    # compute pf
    pf = shepard_v1(sx, sf, px, N_min = N_min, N_max = N_max, epsilon = epsilon)

    # compute weights
    w, sx, sf = weight_v1_all(sx, sf, px, N_min = N_min, N_max = N_max)

    # remove close-by scatters, if no scatters left, gradients equal zero by default
    d = dist_2_all(sx, px)
    di = np.where(d >= epsilon)[0]
    if len(di) == 0:
        return g
    sx = sx[di]
    sf = sf[di]
    d = d[di]

    # compute gradeitns
    for di in np.arange(dim):
        normi = 0
        denormi = np.sum(w**2)
        if(denormi == 0):
            g[di] = 0.0
        else:
            for i in np.arange(len(sx)):
                normi += (w[i]**2) * (sf[i] - pf) * (sx[i, di] - px[di])/(d[i]**2)
            g[di] = normi/denormi
    
    return g

from numpy.linalg import norm
import sys

from shepard import *

# compute coefficient t to include directional effects
def include_direction(i, sx, px, s):
    denormi, normi = 0, 0
    for j in np.arange(len(sx)):
        p1 = (px - sx[j])
        p2 = (px - sx[i])
        k = 1 - (p1 @ p2)/(norm(p1) * norm(p2))
        normi += s[j] * k
        denormi += s[j]
    return normi/denormi

def weight_v2(s, i, sx, px):
    t = include_direction(i, sx, px, s)
    return (s[i]**2) * (1 + t)
    
def weight_v2_all(sx, sf, px, N_min, N_max):
    
    # compute weights
    s, sx, sf = weight_v1_all(sx, sf, px, N_min = N_min, N_max = N_max)
    denormi = np.sum(s)
    if(denormi == 0):
        return s, sx, sf
    else:
        w = np.zeros(len(sx))
        for i in np.arange(len(sx)):
            w[i] = weight_v2(s, i, sx, px)
        return w, sx, sf

def shepard_v2(sx, sf, px, bd = None, N_min = None, N_max = None, epsilon = 1e-3):
      
    # if points close to px exist, compute pf from sf
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    d = dist_2_all(sx, px)
    di = np.where(d < epsilon)[0]
    if(len(di) > 0):
        minidx = di[np.argmin(d[di])]
        return sf[minidx]
    
    # of no close points, compute in shepard v1
    w, sx, sf = weight_v2_all(sx, sf, px, N_min = N_min, N_max = N_max)
    denormi = np.sum(w)
    if(denormi == 0):
        pf = np.average(sf)
    else:
        pf = np.sum(w * sf.T)/denormi
    return pf

# gradient
def gradient_shepard_v2(sx, sf, px, bd = None, N_min = None, N_max = None, epsilon = 1e-3):

    # if bound bd is provided, _normalize sx, px
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    # init gradients
    dim = len(sx[0])
    g = np.zeros(dim)

    # compute pf
    pf = shepard_v2(sx, sf, px, N_min = N_min, N_max = N_max, epsilon = epsilon)

    # remove close-by scatters
    d = dist_2_all(sx, px)
    di = np.where(d >= epsilon)[0]
    if(len(di) == 0):
        return g
    sx = sx[di]
    sf = sf[di]
    d = d[di]
    
    # compute weights
    w, sx, sf = weight_v2_all(sx, sf, px, N_min = N_min, N_max = N_max)
    
    # compute gradeitns
    for di in np.arange(dim):
        normi = 0
        denormi = np.sum(w)
        if(denormi == 0):
            g[di] = 0
        else:
            for i in np.arange(len(sx)):
                normi += w[i] * (sf[i] - pf) * (sx[i, di] - px[di])/(d[i]**2)
            g[di] = normi/denormi
    return g

def compute_dz(sx, sf, px, N_min, N_max, epsilon, factor = 0.1):
    
    """
    Compute slope term delta z, which is used in shepard method version 3
    """

    # init dz, share same size with sf
    dz = sf.copy()

    # distance from px to all sx(s)
    d = dist_2_all(sx, px)

    # interval = (max(sf) - min(sf)) * factor
    inter = (max(sf) - min(sf)) * factor

    # compute per dimension-wise
    dim = len(sx[0])

    for i in np.arange(len(sx)):
        
        # compute gradient
        g = gradient_shepard_v2(sx, sf, sx[i], N_min = N_min, N_max = N_max, epsilon = epsilon)

        # point-wise difference
        diff = px - sx[i]

        # dist step = diff @ g 
        dstep = diff @ g

        # norm of g
        gnorm = np.linalg.norm(g)

        # term v = inter / norm of g
        v = inter / gnorm

        # compute slope term delta z
        dz[i] = dstep * (v / (v + d[i]))

    return dz

def shepard_v3(sx, sf, px, bd = None, N_min = None, N_max = None, factor = 0.1, epsilon = 1e-3):
    
    # if bound (bd) is provided, _normalize sx and px
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    # if any close-by scatter, return it as pf
    d = dist_2_all(sx, px)
    di = np.where(d < epsilon)[0]
    if(len(di) > 0):
        minidx = di[np.argmin(d[di])]
        return sf[minidx]

    # otherwise, compute weights in shepard v2
    w, sx, sf = weight_v2_all(sx, sf, px, N_min = N_min, N_max = N_max)
    
    # compute slope term
    dz = compute_dz(sx, sf, px, N_min = N_min, N_max = N_max, epsilon = epsilon, factor = factor)

    # print('w', w)
    # print('sf', sf)
    # print('dz', dz)
    # print('sf + dz', (sf + dz))
    # compute pf    
    denormi = np.sum(w)
    if(denormi == 0):
        pf = np.average(sf)
    else:
        pf = np.sum(w * (sf + dz).T)/denormi
    return pf

def gradient_shepard_v3(sx, sf, px, bd = None, N_min = None, N_max = None, factor = 0.1, epsilon = 1e-3):

    # _normalize sx and px if bound bd is provided
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    # init gradients
    dim = len(sx[0])
    g = np.zeros(dim)

    # compute pf
    
    import time
    pf = shepard_v3(sx, sf, px, N_min = N_min, N_max = N_max, epsilon = epsilon, factor = factor)
    
    # get rid of near-bys
    d = dist_2_all(sx, px)
    di = np.where(d >= epsilon)[0]
    if len(di) == 0:
        return g
    sx = sx[di]
    sf = sf[di]
    d = d[di]

    # compute weights
    w, sx, sf = weight_v2_all(sx, sf, px, N_min = N_min, N_max = N_max)

    # compute gradients
    for di in np.arange(dim):
        normi = 0
        denormi = np.sum(w**2)
        if(denormi == 0):
            g[di] = 0.0
        else:
            for i in np.arange(len(sx)):
                normi += (w[i]**2) * (sf[i] - pf) * (sx[i, di] - px[di]) / (d[i]**2)
            g[di] = normi / denormi
    return g

def gradient_shepard_v3_1(sx, sf, px, bd = None, N_min = None, N_max = None, u = 1, factor = 0.1, epsilon = 1e-3):

    # _normalize sx and px if bound bd is provided
    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    # init gradients
    dim = len(sx[0])
    g = np.zeros(dim)
    N = len(sx)
    d = dist_2_all(sx, px)
    di = np.where(d >= epsilon)[0]
    if len(di) == 0:
        return g
    sx = sx[di]
    sf = sf[di]
    d = d[di]

    # compute slope term
    dz = compute_dz(sx, sf, px, N_min = N_min, N_max = N_max, epsilon = epsilon, factor = factor)
    zp = sf + dz

    # compute gradients
    d = dist_2_all(sx, px)
    denormi = np.sum(np.power(d, -u))**2
    for di in np.arange(dim):
        for i in np.arange(N):
            for j in np.arange(N):
                if i == j:
                    continue
                g[di] += np.power(d[i], -u-2) * np.power(d[j], -u) * (px[di] - sx[i, di]) * zp[i] * (zp[i] - zp[j])
        g[di] = g[di] / denormi
    return g

def pg2g(pg):
    return np.sqrt(np.sum(pg**2))

def get_fitness(sx, sf, px, bd = None, N_min = 5, N_max = 10, g_alpha = 1.0):

    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    #  vector from px to sx
    v = sx - px
    
    # gradient estimated from shepard surface
    g = g_alpha * gradient_shepard_v3(sx, sf, px, N_min = N_min, N_max = N_max)
    print('g', norm(g))
    
    # modified cos theta 0.5 * (-cos(theta) + 1) between gradient (g) and vector (v)
    cos_theta = 0.5 * (-np.dot(g, v.transpose())/(norm(g) * norm(v, axis = 1)) + 1)

    # fitness weight
    w = norm(v, axis = 1) * (cos_theta**norm(g))
    return w

def get_fitness2(sx, sf, px, bd = None, N_min = 5, N_max = 10, g_alpha = 1.0):

    if bd is not None:
        sx = _normalize(sx, bd)
        px = _normalize(px, bd)[0]

    #  vector from px to sx
    v = sx - px
    # gradient estimated from shepard surface
    g = g_alpha * gradient_shepard_v3(sx, sf, px, N_min = N_min, N_max = N_max)

    # modified cos theta 0.5 * (-cos(theta) + 1) between gradient (g) and vector (v)
    
    test_cos = -np.dot(g, v.transpose())/(norm(g) * norm(v, axis = 1))
    cos_theta = 0.5 * (-np.dot(g, v.transpose())/(norm(g) * norm(v, axis = 1)) + 1)
    # cos theta expression
    power = norm(g)
    if power > 1e3:
        power = 1e3
    elif power < 1e-3:
        power = 1e-3
    cos_theta_exp = cos_theta ** power
   
		# fitness weight 
    w = norm(g) * cos_theta_exp/(max(sf) - min(sf))
    idx = np.argsort(w)[::-1]
    return idx

def get_proj(v, w):
    
    w_norm= norm(w, axis = 1)
    w_unit = np.einsum('ij,i->ij', w, 1/w_norm)
    return np.einsum('ij,ij->i', v, w_unit)

def get_partial(v, w):

    v = np.atleast_2d(v)
    w = np.atleast_2d(w)
    w_norm= norm(w, axis = 1)
    w_unit = np.einsum('ij,i->ij', w, 1/w_norm)
    proj = np.einsum('ij,ij->i', v, w_unit)
    return np.einsum('i,ij->ij', proj, w_unit)

def _double_sort_fitness(a, b):
    
    # sort first list
    idx = a.argsort()[::-1]
    
    # mask of zero fitness
    mask = (a[idx] == 0)
    
    # if mask is not empty
    if len(mask) > 0:
        
        # indices within mask
        zidx = idx[mask]
    
        # sort second list
        idx_b = b[zidx].argsort()
        
        # return new index back to indices sorted by first list
        idx[mask] = zidx[idx_b]
        
    return idx

def select_subset(sx, sf, px, select_f, nset = 20):
    nset = min(len(sx), nset)
    theta = select_f(sx, sf, px)
    xi = theta.argsort()[::-1][:nset]
    return sx[xi], sf[xi]

def select_subset_debug(sx, sf, px, select_f, bd = None, nset = 20, N_min = 5, N_max = 10):
    nset = min(len(sx), nset)
    xi = select_f(sx, sf, px, bd = bd, N_min = N_min, N_max = N_max)
    return sx[xi[:nset]], sf[xi[:nset]]

def select_subset_reverse(sx, sf, px, select_f, nset = 20):
    nset = min(len(sx), nset)
    theta = select_f(sx, sf, px)
    xi = theta.argsort()[:nset]
    return sx[xi], sf[xi]

def select_subset_random(sx, sf, px, select_f, bd = None, nset = 20, g_alpha = 1.0):
    nset = min(len(sx), nset)
    theta = select_f(sx, sf, px, bd = bd, g_alpha = g_alpha)
    sx, sf = draw_N_by_prob(sx, sf, theta, nset)
    return sx, sf

def draw_N_by_prob(sx, sy, w, N):
    
    if sx is None or sy is None:
        return
    
    if len(w) < N:
        return sx, sy
    else:
        sx_n = np.empty((0, len(sx[0])))
        sy_n = []
        while N > 0:
            wcs = np.cumsum(w)
            wcs = wcs/wcs[-1]
            rand_f = np.random.rand(1)
            idx = np.where(wcs > rand_f)[0][0] - 1
            sx_n = np.vstack((sx_n, sx[idx]))
            sy_n = np.hstack((sy_n, sy[idx]))            
            w = np.delete(w, idx)
            sx = np.delete(sx, idx, axis = 0)
            sy = np.delete(sy, idx)
            N = N - 1
        return sx_n, sy_n

import numpy as np
import GPyOpt
import os
import time
from GPyOpt.methods import BayesianOptimization
from matern_som import MaternSOM52
from util import *
import logging

class SomOptPatch(BayesianOptimization):
   
    def __init__(self, dim, f, domain, som = None, X = None, Y = None, gxmin = None, gymin = np.inf, initial_design_numdata = 5, acquisition_type = 'LCB', exact_feval = True, model_update_interval = 1, acquisition_weight = 3, ARD = True, *args, **kwargs):
        
        self.som = som
        kernel_som = None
        if self.som is not None:
            kernel_som = MaternSOM52(2, self.som)

        super(SomOptPatch, self).__init__(f = f, 
                                          domain = domain,
                                          X = X,
                                          Y = Y,
                                          initial_design_numdata = initial_design_numdata,
                                          acquisition_type = acquisition_type,
                                          exact_feval = exact_feval,
                                          model_update_interval = model_update_interval,
                                          kernel = kernel_som, 
                                          *args, **kwargs)

        self.xnew = np.empty(shape = [0, dim])
        self.ynew = np.empty(shape = [0, 1])

        self.y_mean = 0.0
        self.y_std = 1.0

				# logger
        self.logger = logging.getLogger('somoptpatch')
        fh = logging.FileHandler('opt.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        self.logger.info('SOMOpt initialized.')        

        # plot option
        if kwargs['debug']:
            self.debug = True
            if not 'debug_folder' in kwargs:
                if not os.path.isdir('exp'):
                    os.makedirs('exp')
                self.debug_folder = get_active_folder('exp', 'set')
                self.debug_folder = os.path.join(self.debug_folder, 'opt')
                os.makedirs(self.debug_folder)
            else:
                self.debug_folder = kwargs['debug_folder']
                if not os.path.isdir(self.debug_folder):
                    os.makedirs(self.debug_folder)

    def _distance_last_evaluations(self):
        """
        Compute the distance between the last 2 evaluations in real space
        """
        if self.X.shape[0] < 2:
            # less than 2 evaluations
            return np.inf
        x1 = self.som.unmapx(self.X[-2, :])
        x2 = self.som.unmapx(self.X[-1, :])
        d = np.sqrt(np.sum((self.X[-1, :] - self.X[-2, :]) ** 2))
        return d

    def _debug_acq(self, fid):

        if not self.debug:
            return
        
        if fid == 0:
            folder = os.path.join(self.debug_folder, 'acq')
            if not os.path.isdir(folder):
                os.makedirs(folder)
            self.acq_folder = folder

        file_name = os.path.join(self.acq_folder, 'acq' + str(fid))
        self.plot_acquisition(file_name)
        fid += 1
        return fid

    def _update_model_norm(self, normalization_type='stats'):
        """
        Updates the model (when more than one observation is available) and saves the parameters (if available).
        """
        self._update_model(normalization_type = 'stats')
        if self.num_acquisitions % self.model_update_interval == 0:
            self.y_mean = self.Y.mean()
            self.y_std = self.Y.std()        

    def run_optimization(self, max_iter = 0, max_time = np.inf, gxmin = None, gymin = np.inf, eps = 1e-8,
                         context = None, verbosity=False, save_models_parameters= True, report_file = None,
                         evaluations_file = None, models_file=None, debug = True):
        """
        Runs Bayesian Optimization for a number 'max_iter' of iterations (after the initial exploration data)

        :param max_iter: exploration horizon, or number of acquisitions. If nothing is provided optimizes the current acquisition.
        :param max_time: maximum exploration horizon in seconds.
        :param eps: minimum distance between two consecutive x's to keep running the model.
        :param context: fixes specified variables to a particular context (values) for the optimization run (default, None).
        :param verbosity: flag to print the optimization results after each iteration (default, False).
        :param report_file: file to which the results of the optimization are saved (default, None).
        :param evaluations_file: file to which the evalations are saved (default, None).
        :param models_file: file to which the model parameters are saved (default, None).
        """

        t1 = time.time()

        self.logger.info('run_optimization')

        if self.objective is None:
            raise InvalidConfigError("Cannot run the optimization loop without the objective function")

        # --- Save the options to print and save the results
        self.verbosity = verbosity
        self.save_models_parameters = save_models_parameters
        self.report_file = report_file
        self.evaluations_file = evaluations_file
        self.models_file = models_file
        self.model_parameters_iterations = None
        self.context = context

        # --- Check if we can save the model parameters in each iteration
        if self.save_models_parameters == True:
            if not (isinstance(self.model, GPyOpt.models.GPModel) or isinstance(self.model, GPyOpt.models.GPModel_MCMC)):
                print('Models printout after each iteration is only available for GP and GP_MCMC models')
                self.save_models_parameters = False

        # --- Setting up stop conditions
        self.eps = eps
        if  (max_iter is None) and (max_time is None):
            self.max_iter = 0
            self.max_time = np.inf
        elif (max_iter is None) and (max_time is not None):
            self.max_iter = np.inf
            self.max_time = max_time
        elif (max_iter is not None) and (max_time is None):
            self.max_iter = max_iter
            self.max_time = np.inf
        else:
            self.max_iter = max_iter
            self.max_time = max_time

        # --- Initial function evaluation and model fitting
        if self.X is not None and self.Y is None:
            self.Y, cost_values = self.objective.evaluate(self.X)
            if self.cost.cost_type == 'evaluation_time':
                self.cost.update_cost_model(self.X, cost_values)

        # --- Initialize iterations and running time
        self.time_zero = time.time()
        self.cum_time  = 0
        self.num_acquisitions = 0
        self.suggested_sample = None
        self.Y_new = np.inf

        fid = 0
        acq = -np.inf

        # --- Initialize time cost of the evaluations
        while (self.max_time > self.cum_time):

            # --- Update model
            try:
                self._update_model_norm(self.normalization_type)
            except np.linalg.linalg.LinAlgError:
                break

            self.logger.info('opt {%d}', self.num_acquisitions)
            # print('max_iter', self.max_iter)
            if  self.num_acquisitions >= self.max_iter:
                print('terminate! num_acq/max_iter  ', self.num_acquisitions, '/', self.max_iter)
                break
        
            if len(self.X) > 1 and self._distance_last_evaluations() <= self.eps:
                print('terminate! distance/eps      ', self._distance_last_evaluations(), '/', self.eps)
                break

            self.logger.info('acq\gymin: {%.2f}, {%.2f}', acq, gymin)
            self.suggested_sample = self._compute_next_evaluations()

            acq = self.acquisition.acquisition_function(self.suggested_sample)
            # acq = self._acquisition_function(self.suggested_sample)
            acq = acq * self.y_std + self.y_mean
        
            # --- Augment X
            self.X = np.vstack((self.X,self.suggested_sample))

            # --- Evaluate *f* in X, augment Y and update cost function (if needed)
            self.evaluate_objective()

            # m, v = self.model.predict(self.suggested_sample)
 
            if self.Y_new < gymin:
                gxmin = self.suggested_sample
                gymin = self.Y_new

            # --- Record new sample
            self.xnew = np.vstack([self.xnew, self.suggested_sample])
            self.ynew = np.vstack([self.ynew, self.Y_new])
       
            # --- Update current evaluation time and function evaluations
            self.cum_time = time.time() - self.time_zero
            self.num_acquisitions += 1

            t5 = time.time()

            if debug:
                fid = self._debug_acq(fid)

            if verbosity:
              self.logger.info('num acquisition: {}, time elapsed: {:.2f}s'.format(
                self.num_acquisitions, self.cum_time))    

            # --- Stop messages and execution time
            self._compute_results()

            # --- Print the desired result in files
            if self.report_file is not None:
                self.save_report(self.report_file)
            if self.evaluations_file is not None:
                self.save_evaluations(self.evaluations_file)
            if self.models_file is not None:
                self.save_models(self.models_file)

        return gxmin, gymin
 
